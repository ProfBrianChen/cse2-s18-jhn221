//Julia Nelson
//February 16, 2018
//CSE002 Professor Chen
//Lab04 Card Generator

// use a random number generator to pick a number from 1-52
// cards 1-13 represent diamonds, 14-26 represent the clubs, 
// 27-39 represent the hearts, and 40-52 represent the spades
// card identities ascend in step with the card number

public class CardGenerator {
  public static void main(String [] args) {
    int randomNumber = (int) (Math.random() * 52)+1; 
    // generate a random number between 1 and 52
    String suits = " "; 
    String cardIdentity = " ";
    // declare variables for random card
    
    System.out.println(randomNumber);
    
    //asign suits
    if (randomNumber <= 13) {
      suits = "diamonds";
    }
    else if (randomNumber <= 26){
      suits = "clubs";
    }
    else if (randomNumber <=39){
      suits = "hearts";
    }
    else if (randomNumber <= 52){
      suits = "spades";
    }
    
    int numIdentity; // declare variable for idenity
    numIdentity = randomNumber % 13; // use mod to determine the idenity 
    
    switch (numIdentity){ //use switch to determine what identity it is
      case 0:
        cardIdentity = "king";
        break;
      case 1:
        cardIdentity = "queen";
        break;
      case 2:
        cardIdentity = "jack";
        break;
      case 3:
        cardIdentity = "10";
        break;
      case 4:
        cardIdentity = "9";
        break;
      case 5:
        cardIdentity = "8";
        break;
      case 6:
        cardIdentity = "7";
        break;
      case 7:
        cardIdentity = "6";
        break;
      case 8:
        cardIdentity = "5";
        break;
      case 9:
        cardIdentity = "4";
        break;
      case 10:
        cardIdentity = "3";
        break;
      case 11:
        cardIdentity = "2";
        break;
      case 12: 
        cardIdentity = "ace";
        break;   
    }

  System.out.println("You picked the " + cardIdentity + " of " + suits);
}
  } 