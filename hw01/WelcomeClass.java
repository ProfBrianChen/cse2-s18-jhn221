public class WelcomeClass {
  
  public static void main(String[] args) {
    System.out.println("-----------");
    System.out.println("| Welcome |");
    System.out.println("-----------");
    System.out.println(" ^   ^   ^   ^   ^   ^");
    System.out.println("/ \\ / \\ / \\ / \\ / \\ / \\");
    System.out.println("  <-J--H--N--2--2--1->");
    System.out.println("\\ / \\ / \\ / \\ / \\ / \\ /");
    System.out.println(" v   v   v   v   v   v");                   
    
  }
}