// Julia Nelson
// March 6, 2018
// CSE 002 Professor Chen
// Hw05

// write loops that ask the user to enter info relating to a course they're taking
// (course number, dept name, the num times they meet per week, time class starts
// instructor name, num of students) and make sure that what they enter is a correct type
// if not, ask again

import java.util.Scanner;

public class Hw05 {
  public static void main (String[] args) {
    String departmentName=" "; // declare variable and initialize
    int numTimesMeet=0;
    Scanner myScanner = new Scanner (System.in); // import new scanner called myScanner
    System.out.println("Please enter information regarding a course you are taking.");
    
    int courseNum = 0; // declare variable and initialize to 0
    System.out.println("Please enter the course number"); // ask user to input a number
    while (myScanner.hasNextInt() == false){ // if false ask again
      System.out.println("Error: Wrong type. Please enter the course number");
    }
    
    System.out.println("enter a dept name");
    departmentName = myScanner.next();
    while (myScanner.hasNext() == false){ // if false ask again
      System.out.println("Error: Wrong type. Please enter the department name");
    }
    
    System.out.println("Please enter how many times per week the class meets"); // ask user to input a number
    numTimesMeet = myScanner.nextInt(); // declare variable and initialize to 0
    while (myScanner.hasNextInt() == false){ // if false ask again
      System.out.println("Error: Wrong type. Please enter how many times per week the class meets");
    }
    
    System.out.println("Please enter what time the class starts in military time"); // ask user to input a number 
    int time = myScanner.nextInt(); // declare variable and initialize to 0
    while (myScanner.hasNextInt() == false){ // if false ask again
       System.out.println("Error: Wrong type. Please enter what time the class starts in military time");
    }
   
    System.out.println("Please enter the Professor's name");
    String profName = myScanner.next(); // declare variable and initialize    
    while (myScanner.hasNext() == false){ // if false ask again
      System.out.println("Error: Wrong type. Please enter the Professor's name");
    }
    
    System.out.println("Please enter the number of students"); // ask user to input a number
    int numStudents = myScanner.nextInt(); // declare variable and initialize to 0
    while (myScanner.hasNextInt() == false){ // if false ask again
      System.out.println("Error: Wrong type. Please enter the number of students");
    }
        }
 }












