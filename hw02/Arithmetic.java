//
public class Arithmetic {
  public static void main(String [] args) {
    
    int numPants = 3; // number of pants 
    double pantsPrice = 34.98; // cost per pair of pants
    
    int numShirts = 3; // number of sweatshirts
    double shirtPrice = 24.99; // cost per sweatshirt 
    
    int numBelts = 1; // number of betls
    double beltCost = 33.99; // cost per belt
    
    double paSalesTax = 0.06; // the tax rate
    
    double totalCostPants = (numPants * pantsPrice); // total cost of pants
    double totalCostShirts = (numShirts*shirtPrice); // total cost of shirts
    double totalCostBelts = (numBelts*beltCost); // total cost of belts
    
    double salesTaxPants = pantsPrice * paSalesTax; // sales tax charged on pants
    double salesTaxShirts = shirtPrice * paSalesTax; // sales tax charged on shirts
    double salesTaxBelts = beltCost * paSalesTax; // sales tax charged on belts
    
    double salesTaxPantsNew = ((int) (salesTaxPants*100)) /100.0;
    double salesTaxShirtsNew = ((int) (salesTaxShirts*100)) / 100.0;
    double salesTaxBeltsNew = ((int) (salesTaxBelts*100))/100.0;
    
    double totalCostNoTax = totalCostPants + totalCostShirts + totalCostBelts; // total cost without tax
    
    double totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts; //total sales tax
    double totalSalesTaxNew = ((int) (totalSalesTax*100))/100.0;
    
    double totalPaid = totalCostNoTax + totalSalesTax; //total cost paid
    double totalPaidNew = ((int) (totalPaid*100))/100.0;
    
    
    System.out.println("The cost of pants are " + totalCostPants); //print cost of pants
    System.out.println("The cost of shirts are " + totalCostShirts); //print cost of shirts
    System.out.println("The cost of belts are " + totalCostBelts); //print cost of belts
    System.out.println("The sales tax on pants is " + salesTaxPantsNew); //print sales tax on pants
    System.out.println("The sales tax on shirts is " + salesTaxShirtsNew); //print sales tax on shirts
    System.out.println("The sales tax on belts is " + salesTaxBeltsNew); //print sales tax on belts
    System.out.println("The total cost of the purchases before tax is " + totalCostNoTax);
    //print total cost without tax
    System.out.println("The total sales tax is " + totalSalesTaxNew); //print total sales tax
    System.out.println("The total amount paid is " + totalPaidNew); //print total amount paid
    
    
    
    
    
    
    
    
    
    
    

  }
}