// use scanner to obtain from the user the original cost of check,
// the percentage tip they wish to pay, and the number of ways
// the check will be split
// determine how much each person in the group needs to spend

import java.util.Scanner;

public class Check {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println("Enter the orignial cost of the check in the form xx.xx");
    double checkCost = myScanner.nextDouble();
                       
    System.out.println("Enter the percentage tip that you wish to pay as a whole number");   
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //convert the percentage into a decimal
      
    System.out.println("Enter the number of people who went out to dinner");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amt of cost
        dimes, pennies; // for storing digits to right of decimal for cost
    totalCost = checkCost * (1+ tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson*100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars +
                      '.' + dimes + pennies);
    
    
  } // end of main method
} // end of class



