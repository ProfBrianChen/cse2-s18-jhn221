//
public class Cyclometer {
  // main method required for every Java program
  public static void main(String [] args) {
    int secsTrip1=480; // declaring a variable for seconds per trip1 to 480
    int secsTrip2=3220; // declaring an int for seconds per trip 2 to 3220
    int countsTrip1=1561; // declaring an int for rotations per trip 1 to 1561
    int countsTrip2=9037; // delcaring an int for rotations per trip 2 to 9037
    
    double wheelDiameter=27.0, // declaring double to store diameter of wheel
    PI=3.14159, // delcaring double to store value of PI
    feetPerMile=5280, // declaring double variable to store feet per Mile
    inchesPerFoot=12, // declaring double variable to store inches per foot
    secondsPerMinute=60; // storing seconds per minute to 60
    double distanceTrip1, distanceTrip2, totalDistance; // declaring a double variable for 
                                                        // the distances for each trip and total
    
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+ 
                       " minutes and had "+countsTrip1+" counts.");
    
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+ 
                      " minutes and had "+countsTrip2+" counts.");
    
    distanceTrip1=countsTrip1*wheelDiameter*PI; // gives distance in inches
    // for each count, a rotation of the wheel travels the diameter in inches*PI
    
    distanceTrip1/=inchesPerFoot*feetPerMile; //gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    
    totalDistance=distanceTrip1+distanceTrip2;
      
    // Print out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  } // end of main method
} // end of class