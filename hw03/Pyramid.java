// Julia Nelson
// Professor Chen CSE 002
// HW 03 Pyramid.java

import java.util.Scanner; //import scanner

public class Pyramid {
  public static void main(String[] args) {
  
  Scanner myScanner = new Scanner (System.in);
  
  System.out.println("The square side of the pyramid is (input length)");
  // ask user to input side length of pyramid
  int sideLength = myScanner.nextInt();
  // declare user's input as variable sideLength
  int baseArea = sideLength * sideLength;
  // find area of base by squaring the length of the side 
  
  System.out.println("The height of the pyramid is (input height)");
  // ask user to input height of pyramid
  int height = myScanner.nextInt();
  // declare user's input as variable height
  
  int volume = (baseArea * height)/3;
  // calculate volume of pyramid (1/3*B*H)
  System.out.println("The volume inside the pyramid is: "+volume);
  // output the volume of the pyramid
  }
}