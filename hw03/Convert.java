// Julia Nelson
// CSE002
// Professor Chen
// HW03 Convert

import java.util.Scanner; //import new scanner

public class Convert {
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println("Enter the affected area in acres:"); 
    // ask user to input doubles that represent the num of acres of land
    // affected by hurricane precipitation
    double areaAffected = myScanner.nextDouble();
    // save the number inputed as a double called areaAffected
    
    System.out.println("Enter how many inches of rainfall in the affected area:");
    // ask user to input a number that represents the num of inches of rainfall 
    double inchesRainfall = myScanner.nextDouble();
    // save number inputed as double called inchesRainfall
    
    double totalRain = areaAffected * inchesRainfall;
    // double that gives total amount of rain in inches
    double totalRainGallons = totalRain * 27154.285680218086782;
    // convert inches of rain into gallons
    double cubicMiles = (totalRainGallons /1101117147428.6);
    // calculate how many cubic miles of rain there are
    System.out.println("The total amount of rain is " + cubicMiles + " cubic miles");
    // return to user number of cubic miles
    
  }
}